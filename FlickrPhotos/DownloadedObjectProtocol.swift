//
//  DownloadedObjectProtocol.swift
//  FlickrPhotos
//
//  Created by Giacomo Saccardo on 27/08/2014.
//  Copyright (c) 2014 Giacomo Saccardo. All rights reserved.
//

import Foundation

@objc public protocol DownloadedObjectProtocol {
    var url : String { get }
    var previewUrl : String { get }
    var title : String { get }
    var objectID : String { get }
}