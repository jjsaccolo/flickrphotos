//
//  PhotoCollectionViewCell.swift
//  FlickrPhotos
//
//  Created by Giacomo Saccardo on 27/08/2014.
//  Copyright (c) 2014 Giacomo Saccardo. All rights reserved.
//

import Foundation
import UIKit

class PhotoCollectionViewCell : UICollectionViewCell {
    var imageView : UIImageView
    
    required init(coder aDecoder: NSCoder) {
        fatalError("init(coder:) is not supported")
    }
    
    override init(frame: CGRect) {
        
        imageView = UIImageView()
        
        super.init(frame: frame)
        
        imageView.frame = contentView.frame
        imageView.layer.cornerRadius = 2
        imageView.layer.masksToBounds = true
        imageView.backgroundColor = UIColor(white: 0, alpha: 0.1)
        imageView.contentMode = .ScaleAspectFill
        contentView.addSubview(imageView)
    }
    
    func setImageWithURL(url: String) {
        imageView.sd_setImageWithURL(NSURL(string: url), placeholderImage: nil, options: .RetryFailed)
    }
}