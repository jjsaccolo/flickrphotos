//
//  PhotosViewControllerTests.swift
//  FlickrPhotos
//
//  Created by Giacomo Saccardo on 27/08/2014.
//  Copyright (c) 2014 Giacomo Saccardo. All rights reserved.
//

import Foundation
import Quick
import Nimble
import FlickrPhotos

class PhotosViewControllerTests: QuickSpec {
    override func spec() {
        
        describe("PhotosViewController", {
            
            var photosViewController : PhotosViewController?
            beforeEach({
                photosViewController = PhotosViewController(downloader: FlickrDownloader())
            })
            
            it("Should have a PhotosView object as view", {
                let view = photosViewController!.view
                expect(view is PhotosView).to(beTruthy())
            })  
        })
    }
}