//
//  FlickrDownloader.swift
//  FlickrPhotos
//
//  Created by Giacomo Saccardo on 27/08/2014.
//  Copyright (c) 2014 Giacomo Saccardo. All rights reserved.
//

import Foundation

public class FlickrDownloader : DownloaderProtocol {
    let flickrErrorDomain = "com.giacomosaccardo.FlickrDownloaderError"
    private let flickrAPIKey = "163b534b4d1c38f1ba995514c864499a"
    public var recetPhotosURL : String {
        get {
            return "https://api.flickr.com/services/rest/?method=flickr.photos.getRecent&api_key=" + flickrAPIKey + "&format=json"
        }
    }
    
    weak public var delegate: AnyObject? {
        didSet {
            delegate = delegate as? DownloaderDelegate
        }
    }
    
    public init() {
        delegate = nil
    }
    
    public func start() {
        getRecentPhotos { (error) in
            if (error == nil) {
                self.delegate?.downloaderDidFinish?(self)
            }
            else {
                self.delegate?.downloaderDidFail?(self, error: error!)
            }
        }
    }
    
    //MARK : GET methods
    
    public func flickrAPIGET(url: String, completionHandler: (JSON: JSONValue?, error: NSError?) -> Void) {
        /*
        Flickr response is not a well formatted JSON (it's a valid Javascript function because... why not?);
        the structure is "jsonFlickrApi(HERE THE JSON)".
        Because of this, get the JSON from the NSData downloaded will fail: that's why I'm getting the response
        as a String, I'm stripping the extra characters and, finally, creating the JSONValue.
        */
        
        Alamofire.request(.GET, url).responseString { (request, response, responseString, error) -> Void in
            if (error != nil) {
                completionHandler(JSON: nil, error: error)
                return
            }
            
            if let string = responseString {
                var JSONString = string as NSString
                JSONString = JSONString.substringFromIndex(countElements("jsonFlickrApi("))
                JSONString = JSONString.substringToIndex(JSONString.length - 1)
                let data = JSONString.dataUsingEncoding(NSUTF8StringEncoding)
                let JSON = JSONValue(data)
                
                completionHandler(JSON: JSON, error: nil)
            }
            else {
                completionHandler(JSON: nil, error: NSError.errorWithDomain(self.flickrErrorDomain, code: 42, userInfo: ["userInfo" : "String serializer failed to create the string response"]))
            }
        }
    }
    
    public func getRecentPhotos(photosCompletionhandler: (error: NSError?) -> Void) {
        
        delegate?.downloaderDidStart?(self)
        
        flickrAPIGET(self.recetPhotosURL, completionHandler: { (response, error) in
            
            if (response == nil) {
                photosCompletionhandler(error: NSError.errorWithDomain(self.flickrErrorDomain, code: 42, userInfo: ["userInfo" : "GET to \(self.recetPhotosURL) failed"]))
                return
            }
            
            if let photosResponse = response!["photos"]["photo"].array {
                for photoResponse in photosResponse {
                    let photo = self.parsePhoto(photoResponse)
                    self.delegate?.downloaderDidDownloadObject?(self, object: photo)
                }
                
                photosCompletionhandler(error: nil)
            }
            else {
                let resultError = error ?? NSError.errorWithDomain(self.flickrErrorDomain, code: 42, userInfo: ["userInfo" : "Error reading the 'photos' array in the response"])
                photosCompletionhandler(error: resultError)
            }
        })
    }
    
    public func parsePhoto(response: JSONValue) -> DownloadedObjectProtocol {
        let photo = FlickrPhoto(title: response["title"].string!,
            objectID: response["id"].string!,
            server: response["server"].string!,
            farm: response["farm"].number!,
            secret: response["secret"].string!)
        
        return photo
    }
}