# README #

FlickrPhotos lets you browse the latest pictures uploaded on Flickr.

The app is made in Swift, using Xcode 6 beta-6.

**EDIT**: Updated for Xcode 6 beta-7. If you still want to try it with beta-6, checkout `762dbdd`.

### Details

The app is built on the top of three protocols: **DownloaderProtocol**, **DownloadedObjectProtocol** and **DownloaderDelegate**.
The class in charge of download/get images from a different source needs to conform to the DownloaderProtocol: the objects downloaded needs to conform to DownloadedObjectProtocol; finally, the class that will start the download (and eventually display the images) has to conform to DownloaderDelegate.
The idea is that, ideally, creating a new object that conform to DownloaderProtocol and replacing my "FlickrDownloader" would be safe, the app will display the new images.

I said "ideally" because, working on this, I found a bug on Swift: create two protocols that reference to each other cause XCode to don't compile the project. 
I opened a radar, you can read a better explanation here http://openradar.appspot.com/18061131.

As a workaround, DownloaderProtocol has a delegate of type AnyObject? (instead of DownloaderDelegate?): because of this, the setter of this variable looks like this:

```
#!Swift
weak public var delegate: AnyObject? {
    didSet {
        delegate = delegate as? DownloaderDelegate
    }
}

```

Fortunately the Swift compiler is smart enough to recognize that, from now on, the "delegate" variable is of type DownloaderDelegate (awesome!).


### Instructions

1. Run `pod install`
1. Open the workspace
1. Select the `Nimble-iOS` target and compile
1. Select the `FlickrPhotos` target, a iOS 8 simulator and compile
1. Run :)