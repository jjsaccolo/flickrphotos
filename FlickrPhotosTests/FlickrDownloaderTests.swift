//
//  FlickrDownloaderTests.swift
//  FlickrPhotos
//
//  Created by Giacomo Saccardo on 27/08/2014.
//  Copyright (c) 2014 Giacomo Saccardo. All rights reserved.
//

import Foundation
import Quick
import Nimble
import FlickrPhotos

class FlickrDownloaderTests: QuickSpec {
    override func spec() {
        describe("a FlickrDownloader object", {
            
            var downloader : FlickrDownloader?
            beforeEach({
                downloader = FlickrDownloader()
            })
            
            context("Download data", {
                
                it("the flickrAPIGET method should return a valid JSONValue without errors", {
                    
                    var returnedJSON : JSONValue?
                    var returnedError : NSError?
                    downloader!.flickrAPIGET(downloader!.recetPhotosURL, completionHandler: { (JSON, error) in
                        returnedJSON = JSON
                        returnedError = error
                    })
                    
                    expect(returnedJSON).toEventuallyNot(beNil(), timeout: 10)
                    expect(returnedError).toEventually(beNil(), timeout: 10)
                })
                
                it("the gerRecentPhotos method should return data without errors", {
                    var returnedError : NSError?
                    downloader!.getRecentPhotos({ (error) in
                        returnedError = error
                    })
                    
                    expect(returnedError).toEventually(beNil(), timeout: 10)
                })
                
                context("Parse response", {
                    
                    var jsonMock : JSONValue?
                    beforeEach({
                        let photoMock1 = [
                            "owner":"29075648@N02",
                            "ispublic":1,
                            "title":"Membrillos al Vino Tinto 024",
                            "isfamily":0,
                            "id":"14807221699",
                            "farm":4,
                            "server":"3893",
                            "secret":"f1a824f195",
                            "isfriend":0]
                        let photoMock2 = [
                            "owner":"34742867@N05",
                            "ispublic": 1,
                            "title":"5H5A1940-16.jpg",
                            "isfamily":0,
                            "id":"14807222539",
                            "farm":4,
                            "server":"3857",
                            "secret":"9b299231b4",
                            "isfriend":0]
                        let photoMock3 = [
                            "owner":"121240472@N04",
                            "ispublic":1,
                            "title":"quanta maldade RT @Suma_BR: #SomosTodosPennywise #DiadosPais #StephenKing http://t.co/q0L8gN7Ksi",
                            "isfamily":0,
                            "id":"14807222599",
                            "farm":6,
                            "server":"5576",
                            "secret":"0d5fa1ca6a",
                            "isfriend":0]
                        let dictionary = ["stat":"ok", "photos" : ["totoal" : 1000, "page":1, "perpage":100, "pages":10, "photo" : [photoMock1, photoMock2, photoMock3]]]
                        
                        jsonMock = JSONValue(dictionary)
                    })
                    
                    it("Should have a 'photos' dictionary, which should include a 'photo' array", {
                        let photosArray = jsonMock!["photos"]["photo"].array!
                        expect(photosArray).toNot(beNil())
                    })
                    
                    describe("The array of photos parsed in the response", {
                        
                        var photosArray : [JSONValue]?
                        beforeEach({
                            photosArray = jsonMock!["photos"]["photo"].array!
                        })
                        
                        it("Should contain the correct number of objects", {
                            expect(photosArray!.count).to(equal(3))
                        })
                        
                        it("Should contains objects that conform to the DownloadedObjectProtocol protocol", {
                            var array : [DownloadedObjectProtocol] = []
                            for photo in photosArray! {
                                let downloadedObject = downloader!.parsePhoto(photo)
                                array.append(downloadedObject)
                            }
                            //Check "anObject is DownloadedObjectProtocol" throw a compiler error, since it's aslways true
                            //
                            expect(array.count).to(equal(3))
                        })
                    })
                })
            })
        })
    }
}