//
//  PhotosViewController.swift
//  FlickrPhotos
//
//  Created by Giacomo Saccardo on 27/08/2014.
//  Copyright (c) 2014 Giacomo Saccardo. All rights reserved.
//

import Foundation
import UIKit

public class PhotosViewController : UIViewController, DownloaderDelegate, UICollectionViewDelegate, UICollectionViewDataSource, UICollectionViewDelegateFlowLayout {
    
    let downloader : DownloaderProtocol
    var photos : NSMutableOrderedSet
    var photosView : PhotosView { return view as PhotosView }
    let reuseIdentifierText = "PhotosCollectionViewCellReuseIdentifier"
    
    required public init(coder aDecoder: NSCoder) {
        fatalError("init(coder:) is not supported")
    }
    
    public init(downloader: DownloaderProtocol) {
        self.downloader = downloader
        photos = NSMutableOrderedSet()
        
        super.init(nibName: nil, bundle: nil)
        
        self.downloader.delegate = self;
        title = NSLocalizedString("Recent photos", comment: "title of the view controller 'Recent photos'")
    }
    
    override public func loadView() {
        view = PhotosView(frame: UIScreen.mainScreen().bounds, viewController: self)
    }
    
    override public func viewDidLoad() {
        super.viewDidLoad()
        startToDownload()
    }
    
    func refreshControlWasPulled() {
        startToDownload()
    }
    
    func startToDownload() {
        downloader.start()
    }
    
    func downloadDidFinish() {
        photosView.collectionView.reloadData()
        photosView.refreshControl.endRefreshing()
    }
    
    
    //MARK: DownloaderDelegate methods
    public func downloaderDidStart(parser: DownloaderProtocol) {
        if !(photosView.refreshControl.refreshing) {
            //When the download starts from viewDidLoad (the first time), start the UIRefreshControl animation
            photosView.refreshControl.beginRefreshing()
        }
    }
    
    public func downloaderDidFail(parser: DownloaderProtocol, error: NSError) {

        downloadDidFinish()
        
        let alertView = UIAlertController(title: "Error", message: NSLocalizedString("An error occurred. Please try again.", comment: "Error message displayed when the download of the images fails"), preferredStyle: .Alert)
        alertView.addAction(UIAlertAction(title: "Ok", style: .Default, handler: nil))
        presentViewController(alertView, animated: true, completion: nil)
    }
    
    public func downloaderDidDownloadObject(parser: DownloaderProtocol, object: DownloadedObjectProtocol) {
        photos.insertObject(object, atIndex: 0)
    }
    
    public func downloaderDidFinish(parser: DownloaderProtocol) {
        downloadDidFinish()
    }
    
    
    //MARK: UICollectionViewDataSource methods
    public func collectionView(collectionView: UICollectionView, numberOfItemsInSection section: Int) -> Int {
        return photos.count
    }
    
    public func collectionView(collectionView: UICollectionView, cellForItemAtIndexPath indexPath: NSIndexPath) -> UICollectionViewCell {
        let cell = collectionView.dequeueReusableCellWithReuseIdentifier(reuseIdentifierText, forIndexPath: indexPath) as PhotoCollectionViewCell
        
        let photosArray = photos.array as [DownloadedObjectProtocol]
        cell.setImageWithURL(photosArray[indexPath.row].previewUrl)
        
        return cell
    }
    
    
    //MARK: UICollectionViewDelegate methods
    public func collectionView(collectionView: UICollectionView!, didSelectItemAtIndexPath indexPath: NSIndexPath!) {
        let photosArray = photos.array as [DownloadedObjectProtocol]
        let viewController = SinglePhotoViewController(photo: photosArray[indexPath.row])
        navigationController?.pushViewController(viewController, animated: true)
    }
}
