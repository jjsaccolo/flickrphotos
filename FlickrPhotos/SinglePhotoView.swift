//
//  SinglePhotoView.swift
//  FlickrPhotos
//
//  Created by Giacomo Saccardo on 27/08/2014.
//  Copyright (c) 2014 Giacomo Saccardo. All rights reserved.
//

import Foundation
import UIKit

public class SinglePhotoView : UIView {
    
    let titleLabel : UILabel
    let imageView : UIImageView
    
    public required init(coder aDecoder: NSCoder) {
        fatalError("init(coder:) has not been implemented")
    }
    
    public override init(frame: CGRect) {
        
        titleLabel = UILabel(frame: CGRectNull)
        imageView = UIImageView(frame: CGRectNull)
        
        super.init(frame: frame)
        
        titleLabel.backgroundColor = UIColor.whiteColor()
        titleLabel.font = UIFont.systemFontOfSize(18)
        titleLabel.textColor = UIColor.darkGrayColor()
        titleLabel.numberOfLines = 2
        addSubview(titleLabel)
        
        imageView.contentMode = .ScaleAspectFit
        addSubview(imageView)
        
    }
    
    public override func layoutSubviews() {
        super.layoutSubviews()
        
        titleLabel.sizeToFit()
        titleLabel.frame = CGRect(x: 10, y: 80, width: 300, height:44)
        
        imageView.frame = CGRect(x: 10, y: 140, width: 300, height: 390)
    }
    
    func setTitle(title: String) {
        titleLabel.text = title
    }
    
    func setImageWithURL(url: String) {
        imageView.setImageWithURL(NSURL(string: url), placeholderImage: nil, usingActivityIndicatorStyle: .Gray)
    }
}
