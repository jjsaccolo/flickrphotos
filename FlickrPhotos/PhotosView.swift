//
//  PhotosView.swift
//  FlickrPhotos
//
//  Created by Giacomo Saccardo on 27/08/2014.
//  Copyright (c) 2014 Giacomo Saccardo. All rights reserved.
//

import Foundation
import UIKit

public class PhotosView : UIView {
    let collectionView : UICollectionView
    let refreshControl : UIRefreshControl

    public required init(coder aDecoder: NSCoder) {
        fatalError("init(coder:) is not supported")
    }
    
    public init(frame: CGRect, viewController: PhotosViewController) {
        let flowLayout = UICollectionViewFlowLayout()
        flowLayout.itemSize = CGSize(width: 60, height: 60)
        flowLayout.sectionInset = UIEdgeInsets(top: 25, left: 25, bottom: 25, right: 25)
        collectionView = UICollectionView(frame: CGRectNull, collectionViewLayout: flowLayout)
        refreshControl = UIRefreshControl()

        super.init(frame: frame)
        
        collectionView.backgroundColor = UIColor.whiteColor()
        collectionView.alwaysBounceVertical = true
        collectionView.delegate = viewController
        collectionView.dataSource = viewController
        collectionView.registerClass(PhotoCollectionViewCell.self, forCellWithReuseIdentifier: viewController.reuseIdentifierText)
        addSubview(collectionView)
        
        refreshControl.addTarget(viewController, action: "refreshControlWasPulled", forControlEvents: .ValueChanged)
        collectionView.addSubview(refreshControl)
    }
    
    public override func layoutSubviews() {
        super.layoutSubviews()
        
        collectionView.frame = frame
    }
}