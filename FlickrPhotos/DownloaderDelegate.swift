//
//  DownloaderDelegate.swift
//  FlickrPhotos
//
//  Created by Giacomo Saccardo on 27/08/2014.
//  Copyright (c) 2014 Giacomo Saccardo. All rights reserved.
//

import Foundation

@objc public protocol DownloaderDelegate : class {
    optional func downloaderDidStart(parser: DownloaderProtocol)
    optional func downloaderDidDownloadObject(parser: DownloaderProtocol, object: DownloadedObjectProtocol)
    optional func downloaderDidFinish(parser: DownloaderProtocol)
    optional func downloaderDidFail(parser: DownloaderProtocol, error: NSError)
}