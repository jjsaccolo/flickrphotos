//
//  SinglePhotoViewController.swift
//  FlickrPhotos
//
//  Created by Giacomo Saccardo on 27/08/2014.
//  Copyright (c) 2014 Giacomo Saccardo. All rights reserved.
//

import Foundation
import UIKit

public class SinglePhotoViewController : UIViewController {
    
    var singlePhotoView : SinglePhotoView { return view as SinglePhotoView }
    let photo : DownloadedObjectProtocol
    
    public required init(coder aDecoder: NSCoder) {
        fatalError("init(coder:) is not supported")
    }
    
    public init(photo: DownloadedObjectProtocol) {
        self.photo = photo
        
        super.init(nibName: nil, bundle: nil)
        
        view.backgroundColor = UIColor.whiteColor()
        title = NSLocalizedString("Photo", comment: "Single photo")
    }
    
    public override func loadView() {
        view = SinglePhotoView(frame: UIScreen.mainScreen().bounds)
    }
    
    public override func viewDidLoad() {
        super.viewDidLoad()
        
        singlePhotoView.setTitle(photo.title)
        singlePhotoView.setImageWithURL(photo.url)
    }
}