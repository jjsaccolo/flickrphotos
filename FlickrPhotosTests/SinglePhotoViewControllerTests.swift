//
//  SinglePhotoViewController.swift
//  FlickrPhotos
//
//  Created by Giacomo Saccardo on 27/08/2014.
//  Copyright (c) 2014 Giacomo Saccardo. All rights reserved.
//

import Foundation
import Quick
import Nimble
import FlickrPhotos

class SinglePhotoViewControllerTests: QuickSpec {
    override func spec() {
        
        describe("SinglePhotoViewController", {
            
            var singlePhotosViewController : SinglePhotoViewController?
            beforeEach({
                let photo = FlickrPhoto(title: "title", objectID: "objectID", server: "server", farm: 42, secret: "secret")
                singlePhotosViewController = SinglePhotoViewController(photo: photo)
            })
            
            it("Should have a SinglePhotoView object as view ", {
                let view = singlePhotosViewController?.view
                expect(view is SinglePhotoView).to(beTruthy())
            })
        })
    }
}