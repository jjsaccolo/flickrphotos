//
//  FlickrPhoto.swift
//  FlickrPhotos
//
//  Created by Giacomo Saccardo on 27/08/2014.
//  Copyright (c) 2014 Giacomo Saccardo. All rights reserved.
//

import Foundation

public class FlickrPhoto : NSObject, DownloadedObjectProtocol {
    
    public var url : String
    public var previewUrl : String
    public var title : String
    public var objectID : String
    
    public init(title: String, objectID: String, server: String, farm: Int, secret: String) {
        self.title = title
        self.objectID = objectID
        
        let baseUrl = "https://farm\(farm).staticflickr.com/\(server)/\(objectID)_\(secret)"
        
        self.url = "\(baseUrl).jpg"
        self.previewUrl = "\(baseUrl)_q.jpg"
    }
    
    //MARK: Equality
    
    func isEqualToPhoto(photo: FlickrPhoto) -> Bool {
        let haveEqualIDs = self.objectID == photo.objectID
        return haveEqualIDs
    }
    
    override public func isEqual(object: AnyObject!) -> Bool {
        
        if (self === object) {
            return true
        }
        
        if !(object is FlickrPhoto) {
            return false
        }
        
        return self.isEqualToPhoto(object as FlickrPhoto)
    }
    
    override public var hash : Int {
        get {
            return url.hashValue ^ objectID.hashValue
        }
    }
    
}