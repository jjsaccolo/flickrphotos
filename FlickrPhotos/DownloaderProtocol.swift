//
//  DownloaderProtocol.swift
//  FlickrPhotos
//
//  Created by Giacomo Saccardo on 27/08/2014.
//  Copyright (c) 2014 Giacomo Saccardo. All rights reserved.
//

import Foundation

@objc public protocol DownloaderProtocol {
    func start()
    
    /*
      Please, read the README file to understand why I'm using a delegate of type AnyObject?
      and not of type DownloaderDelegate? (which should be better).
    */
    @objc weak var delegate: AnyObject? { get set }
}