//
//  FlickrPhotoTests.swift
//  FlickrPhotos
//
//  Created by Giacomo Saccardo on 27/08/2014.
//  Copyright (c) 2014 Giacomo Saccardo. All rights reserved.
//

import Foundation
import Quick
import Nimble
import FlickrPhotos

class FlickrPhotoTests: QuickSpec {
    override func spec() {
        describe("a FlickrPhoto object", {
            
            var photo : FlickrPhoto?
            beforeEach({
                photo = FlickrPhoto(title: "title", objectID: "objectID", server: "server", farm: 42, secret: "secret")
            })
            
            it("should have a url and a previewUrl", {
                expect(photo!.url).toNot(beNil())
                expect(photo!.previewUrl).toNot(beNil())
            })
            
            it("should have a valid url", {
                expect(photo!.url).to(equal("https://farm42.staticflickr.com/server/objectID_secret.jpg"))
            })

            it("should have a valid previewUrl", {
                expect(photo!.previewUrl).to(equal("https://farm42.staticflickr.com/server/objectID_secret_q.jpg"))
            })
        })
    }
}